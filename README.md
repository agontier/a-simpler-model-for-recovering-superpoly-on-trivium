# Git repository for the paper A Simpler Model for Recovering Superpoly on Trivium

To run a trivium instance go to a directory and type 
```make R``` with R the round numbers (675, 735, 840, 841, 842)

**Remarks**
maven is needed to compile the CP model

Gurobi is needed (with a licence) to compile the MILP model (dont forget to modify the gurobi library path in the makefile)

# CP Graph model

```
SaveChoco
          Makefile          - compile or/and solve all round cubes on one thread
          pom.xml.          - Maven project file
          \src\main\java
                        \propagators      - various propagators for the graph model
                        TriviumGraph.java - Main file where the Graph model is declared and solved
                        \utils            - Additional Graph structures
          \target           - compiled code location
```

**Remarks**
The Makefile only run on 1 thread. To solve on parallel run 

```
java -Djava.util.concurrent.ForkJoinPool.common.parallelism=T -jar ./target/graphit-1.0-SNAPSHOT-jar-with-dependencies.jar -r R -i IV
```
with T the thread number, R the round number and IV the cube name (for example iv_675_1)

# MILP Graph model

```
SaveGurobi
          Makefile          - compile or/and solve all round cubes (multithreading is handeled by gurobi up to 32 threads)
          milptrivium.cpp   - source code
          thune.prm         - Gurobi parameters
```
