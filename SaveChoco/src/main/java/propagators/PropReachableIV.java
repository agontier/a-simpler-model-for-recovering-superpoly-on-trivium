/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import gnu.trove.map.hash.TIntObjectHashMap;
import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.graphsolver.variables.delta.GraphDeltaMonitor;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.solver.variables.events.PropagatorEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.procedure.IntProcedure;
import org.chocosolver.util.procedure.PairProcedure;
import utils.Registre;

import java.util.ArrayDeque;
import java.util.BitSet;
import java.util.Deque;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 25/11/2020
 */
public class PropReachableIV extends Propagator<Variable> {

    final DirectedGraphVar graph;
    final DirectedGraph d; // doublies
    final DirectedGraph longs;
    final DirectedGraph loops;
    final IntVar[] RIV;
    private final GraphDeltaMonitor gdm;
    private final IntProcedure nodeEvent;
    private final PairProcedure arcEvent;
    private TIntObjectHashMap<Registre> nodReg;
    private Deque<Integer> stack = new ArrayDeque<>();
    private BitSet stacked = new BitSet();
    private int sink;


    public PropReachableIV(DirectedGraphVar g, DirectedGraph doublements, DirectedGraph longs, DirectedGraph loops, IntVar[] reachableIV, TIntObjectHashMap<Registre> nodReg, int sink) {
        super(new DirectedGraphVar[]{g}, PropagatorPriority.BINARY, true);
        this.graph = g;
        this.sink = sink;
        this.d = doublements;
        this.RIV = reachableIV;
        this.longs = longs;
        this.nodReg = nodReg;
        this.loops = loops;
        this.gdm = g.monitorDelta(this);
        this.nodeEvent = i -> {
            for (int p : graph.getPotPredOf(i))
                push(p);
            update(i);
        };
        this.arcEvent = (i, j) -> {
            for (int p : graph.getPotPredOf(i))
                push(p);
            update(i);
        };
    }

    private void push(int i) {
        if (!stacked.get(i)) {
            stack.add(i);
            stacked.set(i);
        }
    }

    private int pop() {
        int i = stack.pop();
        stacked.clear(i);
        return i;
    }

    private void update(int i) throws ContradictionException {
        push(i);
//        filter();
        forcePropagate(PropagatorEventType.CUSTOM_PROPAGATION);
    }

    private void filter() throws ContradictionException {
        while (!stack.isEmpty()) {
            int i = pop();
            int max = max_succ_smart(i);
            if (RIV[i].getUB() > max) {
//                cheklb(i);
                add_pred(i);
                if (RIV[i].getLB() > max)
                    graph.removeNode(i, this);
                else
                    RIV[i].updateUpperBound(max, this);
            }
        }
    }

    private int max_succ(int i) {
        int max = 0;
        int sum = 0;
        for (int s : graph.getPotSuccOf(i)) {
            if (d.arcExists(i, s))
                sum += RIV[s].getUB();
            max = Math.max(max, RIV[s].getUB());
        }
        max = Math.max(max, sum);
        return max;
    }

    private int max_succ_smart(int i) {
        int max = 0;
        if (!graph.getPotPredOf(sink).contains(i)) {
            for (int s : graph.getPotSuccOf(i)) {
                if (!d.arcExists(i, s))
                    max = Math.max(max, RIV[s].getUB());
            }
            if (d.getSuccOf(i).size() > 0) {
                int s4 = d.getSuccOf(i).max();
                int s5 = d.getSuccOf(i).min();
                if (nodReg.get(s4).round < nodReg.get(s5).round) {
                    int tmp = s4;
                    s4 = s5;
                    s5 = tmp;
                }
                if (graph.getPotSuccOf(i).contains(s4) && graph.getPotSuccOf(i).contains(s5)) {
                    if (graph.getPotPredOf(sink).contains(s4) || graph.getPotPredOf(sink).contains(s5)) {
                        max = Math.max(max, RIV[s4].getUB() + RIV[s5].getUB());
                    } else {
                        for (int s4s : graph.getPotSuccOf(s4)) {
                            if (!d.arcExists(s4, s4s)) {
                                if (!longs.arcExists(s4, s4s)) {
                                    max = Math.max(max, RIV[s4s].getUB() + RIV[s5].getUB());
                                } else {//s4s est l'arc long
                                    int maxs5lin = 0;
                                    for (int s5s : graph.getPotSuccOf(s5)) {
                                        if (!d.arcExists(s5, s5s)) {
                                            maxs5lin = Math.max(maxs5lin, RIV[s5s].getUB());
                                        }
                                    }
                                    maxs5lin += RIV[s4s].getUB();
                                    max = Math.max(max, maxs5lin);
                                    int maxs5dou = 0;
                                    if (d.getSuccOf(s5).size() == 2) {
                                        int s5s4 = d.getSuccOf(s5).max();
                                        int s5s5 = d.getSuccOf(s5).min();
                                        if (nodReg.get(s5s4).round < nodReg.get(s5s5).round) {
                                            int tmp = s5s4;
                                            s5s4 = s5s5;
                                            s5s5 = tmp;
                                        }
                                        if (graph.getPotSuccOf(s5).contains(s5s4) && graph.getPotSuccOf(s5).contains(s5s5)) {
                                            maxs5dou = RIV[s5s4].getUB() + RIV[s5s5].getUB();
                                            assert s4s == s5s5 : s4s + " " + s5s4 + " " + s5s5;
                                        }
                                    }
                                    max = Math.max(max, maxs5dou);
                                }
                            }
                        }
                        if (d.getSuccOf(s4).size() == 2) {
                            int s4s4 = d.getSuccOf(s4).max();
                            int s4s5 = d.getSuccOf(s4).min();
                            if (nodReg.get(s4s4).round < nodReg.get(s4s5).round) {
                                int tmp = s4s4;
                                s4s4 = s4s5;
                                s4s5 = tmp;
                            }
                            if (graph.getPotSuccOf(s4).contains(s4s4) && graph.getPotSuccOf(s4).contains(s4s5)) {
                                int maxs5lin = 0;
                                for (int s5s : graph.getPotSuccOf(s5)) {
                                    if (!d.arcExists(s5, s5s)) {
                                        maxs5lin = Math.max(maxs5lin, RIV[s5s].getUB());
                                    }
                                }
                                maxs5lin += RIV[s4s4].getUB() + RIV[s4s5].getUB();
                                max = Math.max(max, maxs5lin);
                                int maxs5dou = 0;
                                if (d.getSuccOf(s5).size() == 2) {
                                    int s5s4 = d.getSuccOf(s5).max();
                                    int s5s5 = d.getSuccOf(s5).min();
                                    if (nodReg.get(s5s4).round < nodReg.get(s5s5).round) {
                                        int tmp = s5s4;
                                        s5s4 = s5s5;
                                        s5s5 = tmp;
                                    }
                                    if (graph.getPotSuccOf(s5).contains(s5s4) && graph.getPotSuccOf(s5).contains(s5s5)) {
                                        maxs5dou = RIV[s4s4].getUB() + RIV[s4s5].getUB() + RIV[s5s5].getUB();
                                    }
                                }
                                max = Math.max(max, maxs5dou);
                            }
                        }
                    }
                }
            }
        } else {
            max = RIV[i].getUB();
        }
        return max;
    }

    private void add_pred(int i) {
        for (int p : graph.getPotPredOf(i)) {
            push(p);
            for (int pp : graph.getPotPredOf(p)) {
                push(pp);
            }
        }
    }

    private void cheklb(int i) throws ContradictionException {
        int lb = RIV[i].getLB();
        if (lb > 0) {
            //graph.enforceNode(i, this);
            // check simple arc
            for (int s : graph.getPotSuccOf(i)) {
                if (!d.arcExists(i, s) && RIV[s].getUB() < lb) {
                    graph.removeArc(i, s, this);
                }
            }
            // check doublies
            if (d.getSuccOf(i).size() > 0) {
                int s4 = d.getSuccOf(i).max();
                int s5 = d.getSuccOf(i).min();
                // to check
                if (graph.getPotSuccOf(i).contains(s4) && graph.getPotSuccOf(i).contains(s5)) {
                    if (RIV[s4].getUB() + RIV[s5].getUB() < lb) {
                        graph.removeArc(i, s4, this);
                        graph.removeArc(i, s5, this);
                    }
                }
            }
        }
    }

    /*
            a i = max(l i, d i)
            l i = max(a i1, a i2, a i3)
            d i = max(a i41 + a i5,
                      a i42 + a i5,
                      a i43 + l i5, d i5,                                 si on a pris l'arc long on retombe sur le grand des doublons de i5
                      a i44 + a i45 + l i5, a i44 + a i45 + a i55)        si on double deux fois d'afilé alors le fils du milieu est double

                            t = d i44 + a i451 + a i452 + d i55
    */
    @Override
    public int getPropagationConditions(int vIdx) {
        return GraphEventType.REMOVE_ARC.getMask() + GraphEventType.ADD_NODE.getMask();
    }

    @Override
    public void propagate(int idxVarInProp, int mask) throws ContradictionException {
        gdm.freeze();
        gdm.forEachArc(arcEvent, GraphEventType.REMOVE_ARC);
        gdm.forEachNode(nodeEvent, GraphEventType.ADD_NODE);
        gdm.unfreeze();
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        if (PropagatorEventType.isFullPropagation(evtmask)) {
            for (int n : graph.getPotentialNodes())
                if (n != sink) push(n);
        }
        try {
            filter();
            gdm.unfreeze();
        } catch (ContradictionException c) {
            stack.clear();
            stacked.clear();
            throw c;
        }
    }

    @Override
    public ESat isEntailed() {
        if (graph.isInstantiated()) {
            for (int n : graph.getMandatoryNodes()) {
                if (n != sink && RIV[n].getUB() > max_succ_smart(n))
                    return ESat.FALSE;
            }
            return ESat.TRUE;
        }
        return ESat.TRUE;
    }
}
