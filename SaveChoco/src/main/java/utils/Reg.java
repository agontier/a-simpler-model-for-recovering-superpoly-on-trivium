package utils;

public enum Reg {
    A(0, 93), B(93, 84), C(177, 111),
    //B(0, 127),
    S(128, 127);
    ;

    public int pos;
    public int siz;

    Reg(int f, int s) {
        this.pos = f;
        this.siz = s;
    }
}
