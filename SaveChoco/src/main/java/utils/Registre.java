package utils;

import java.util.Objects;

public class Registre implements Comparable<Registre> {
    private static final int UNDECLARED = -1;
    public final int round;
    public final Reg reg;
    public int node = UNDECLARED;

    public Registre(int round, Reg reg) {
        this.round = round;
        this.reg = reg;
    }

    public void setNode(int n) {
        this.node = n;
    }

    public Reg getReg() {
        return reg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registre registre = (Registre) o;
        return round == registre.round &&
                reg == registre.reg;
    }

    @Override
    public int hashCode() {
        return Objects.hash(round, reg);
    }

    @Override
    public int compareTo(Registre o) {
        int dr = this.round - o.round;
        if (dr == 0) {
            return this.reg.pos - o.reg.pos;
        }
        return dr;
    }

    @Override
    public String toString() {
        return reg + "_" + round + ":" + node;
    }

    public String asRegister() {
        return reg + "_" + round;
    }
}
